# Use a multi-stage build for efficiency
FROM golang:1.19.2-alpine AS builder

# Set working directory for build context
WORKDIR /app

# Copy Go source code from current directory
COPY . .

# Install dependencies
RUN go mod download

# Build the Go binary
RUN go build -o jk-golang-webapp-books main.go

# Use a slimmer image for the final artifact
FROM alpine:3.16

# Copy the built binary from the builder stage
COPY --from=builder /app/jk-golang-webapp-books .

# Set the entrypoint to run the binary
ENTRYPOINT ["./jk-golang-webapp-books"]

# Expose the port that your application
EXPOSE $PORT

